<?php

use Tests\TestCase;
use App\Http\Controllers\DataController;

class ArticleIndexTest extends TestCase {
	public function testIndexReturnsText() : void {
		$request = $this->createRequest( 'GET', '/' );
		$res     = $this->app->handle( $request );

		self::assertEquals( 'try /articles OR /articles/{id}', (string) $res->getBody() );
	}

	public function testArticles() : void {
		$request = $this->createRequest( 'GET', '/articles' );
		$res     = $this->app->handle( $request );
		$dc = new DataController();
		$articles = json_encode($dc->articles);

		self::assertEquals( $articles, (string) $res->getBody());
	}

	public function testArticle() : void {
		$request = $this->createRequest( 'GET', '/articles/1' );
		$res     = $this->app->handle( $request );
		$dc = new DataController();
		$article = json_encode($dc->articles[1]);

		self::assertEquals( $article, (string) $res->getBody());
	}

}	
