<?php 

namespace App\Http\Controllers;

// use PHPHtmlParser\Dom;
use KubAT\PhpSimple\HtmlDomParser;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class DataController
{

	public function __construct()
	{
		try{
			$this->articles = file_get_contents(__DIR__.'/../../../assets/articles.json');
			$this->articles = json_decode($this->articles);
			$this->initArticles();
		}catch(Exception $e){
			return $e->getMessage();
		}
	}

	public function initArticles():void
	{
		$processedData = [];
		foreach($this->articles as $article):
			$content = [];
			$htmlData = $this->processHtml($article->content, $article->id);
			$newData = array(
				"id" => $article->id,
				"title" => $article->title,
				"slug" => $article->slug,
				"content" => $htmlData
			);
			$processedData[$article->id] = $newData;
		endforeach;
		$this->articles = $processedData;
	}

	public function getArticles(Request $request, Response $response, $args)
	{
		$response->getBody()->write(json_encode($this->articles));
		return $response
			->withHeader('Content-Type', 'application/json');
	}

	public function getArticle(Request $request, Response $response, $args)
	{
		$route = $request->getAttribute("__route__");
		$articleId = $route->getArgument("id");
		$article = isset($this->articles[$articleId]) ? json_encode($this->articles[$articleId]) : "Article does not exist";
		$response->getBody()->write($article);
		return $response
			->withHeader('Content-Type', 'application/json');
	}

	public function processHtml($content, $id)
	{	
		$htmlData = $content ? HtmlDomParser::str_get_html($content) : '';
		$processed = [];
		if($htmlData):
			$paragraphs = $htmlData->find('p');
			$divs = $htmlData->find('div');
			$images = $htmlData->find('img');
			foreach($paragraphs as $paragraph):
				if(!$paragraph->tag_start):
					$temp = array(
						"type" => "paragraph",
						"content" => htmlspecialchars($paragraph->xmltext())
					);
					array_push($processed, $temp);
				endif;
			endforeach;
			//if there is a root div (not applicable)
			foreach($divs as $div):
				if(!$div->tag_start):
					$temp = array(
						"type" => "div",
						"content" => htmlspecialchars($paragraph->xmltext())
					);
					array_push($processed, $temp);
				endif;
			endforeach;
			//if there is a root img (not applicable)
			foreach($images as $img):
				if(!$img->tag_start):
					$temp = array(
						"type" => "img",
						"content" => htmlspecialchars($paragraph->xmltext())
					);
					array_push($processed, $temp);
				endif;
			endforeach;
		endif;

		if(count($processed)){
			return (object) $processed;
		}else{
			return htmlspecialchars($content);
		}
	}

}