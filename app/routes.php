<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Http\Controllers\DataController;


/** @noinspection PhpUndefinedVariableInspection */
$app->get( '/', function ( Request $request, Response $response, $args ) {
	$response->getBody()->write("try /articles OR /articles/{id}");
	return $response;
});

$app->get( '/articles', DataController::class . ':getArticles');

$app->get('/articles/{id}', DataController::class . ':getArticle');